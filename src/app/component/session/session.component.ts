import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigurationService, PokerSession } from '../../service/configuration.service';
import { PokerMessage } from '../../service/poker.service';

@Component({
  selector: 'poker-session',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './session.component.html',
  styleUrl: './session.component.css'
})
export class SessionComponent {

  modifySessionDialog = false;

  @Input({ required: true }) session!: PokerSession;

  @Output() deleteSession = new EventEmitter<PokerSession>();
  @Output() pokerMessage = new EventEmitter<PokerMessage>();

  constructor(private configuration: ConfigurationService) {
  }

  updateSessionName(name: string) {
    this.configuration.updateSessionName(this.session.key, name);
    this.modifySessionDialog = false;
  }

  sendMessage(message: string) {
    this.pokerMessage.emit({session: this.session, message});
  }

}
