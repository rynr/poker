import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { v4 } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {
  readonly localStorageKey = 'pokerФ';
  private _defaultUsername = new BehaviorSubject<string | undefined>(undefined);
  private _sessions = new BehaviorSubject<Array<PokerSession> | undefined>(undefined);

  get defaultUsername(): Observable<string | undefined> {
    return this._defaultUsername.asObservable();
  }
  get sessions(): Observable<Array<PokerSession> | undefined> {
    return this._sessions.asObservable();
  }

  constructor() {
    const config: PokerConfiguration = JSON.parse(localStorage.getItem(this.localStorageKey) || '{}');
    this._defaultUsername.next(config.defaultUsername);
    this._sessions.next(config.sessions);
  }

  private storeConfiguration() {
    return localStorage.setItem(this.localStorageKey, JSON.stringify({
      defaultUsername: this._defaultUsername.value,
      sessions: this._sessions.value
    }));
  }

  setDefaultUsername(defaultUsername: string) {
    this._defaultUsername.next(defaultUsername);
    this.storeConfiguration();
  }

  createNewSession(name: string): PokerSession {
    const session: PokerSession = { name, key: v4() };
    const sessions = this._sessions.value || [];
    sessions.push(session);
    this._sessions.next(sessions);
    this.storeConfiguration();
    return session;
  }

  updateSessionName(sessionKey: string, name: string) {
    const sessions = this._sessions.value || [];
    sessions.forEach(s => {
      if (s.key === sessionKey) s.name = name;
    })
    this._sessions.next(sessions);
    this.storeConfiguration();
  }

  deleteSession(session: PokerSession) {
    const sessions = this._sessions.value || [];
    sessions.forEach((s, i) => {
      if (s.key === session.key) sessions.splice(i, 1)
    })
    this._sessions.next(sessions);
    this.storeConfiguration();
  }

}

export interface PokerSession {
  key: string;
  name: string;
}

export interface PokerConfiguration {
  defaultUsername?: string;
  sessions?: Array<PokerSession>;
}