import { Injectable } from '@angular/core';
import { PokerSession } from './configuration.service';

@Injectable({
  providedIn: 'root'
})
export class PokerService {
  readonly config = {
    iceServers: [{ urls: "stun:stun.services.mozilla.com:3478" }],
  };
  readonly peerConnection: RTCPeerConnection;
  readonly sessions: Map<PokerSession, RTCDataChannel>;

  constructor() {
    this.peerConnection = new RTCPeerConnection(this.config);
    this.sessions = new Map();
  }

  connect(session: PokerSession) {
    console.log("Connect", session);
    const dataChannel = this.peerConnection.createDataChannel(session.key);
    dataChannel.addEventListener("open", (event) => {
      console.log("Open", event);
    });
    dataChannel.addEventListener("message", (event) => {
      console.group("Message", event);
    })
    dataChannel.addEventListener("error", (event) => {
      console.group("Error", event);
    })
    this.sessions.set(session, dataChannel);
  }

  disconnect(channel: PokerSession) {
    const dataChannel = this.sessions.get(channel);
    if (dataChannel) {
      dataChannel.close();
    }
    this.sessions.delete(channel);
  }

  sendMessage(message: PokerMessage) {
    const dataChannel = this.sessions.get(message.session);
    if (dataChannel) {
      dataChannel.send(message.message);
    } else {
      console.log("Could not send message");
    }
  }
}

export interface PokerMessage {
  session: PokerSession;
  message: string;
}
