import { Routes } from '@angular/router';
import { HomeComponent } from './container/home/home.component';

export const routes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'join/:session', component: HomeComponent}
];
