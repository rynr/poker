import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { RouterLink } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { ConfigurationService, PokerSession } from '../../service/configuration.service';
import { SessionComponent } from '../../component/session/session.component';
import { PokerMessage, PokerService } from '../../service/poker.service';

@Component({
  selector: 'poker-home',
  standalone: true,
  imports: [
    CommonModule, RouterLink,
    SessionComponent
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {

  defaultUsername = new BehaviorSubject<string | undefined>(undefined);
  sessions = new BehaviorSubject<Array<PokerSession> | undefined>(undefined);

  constructor(private configuration: ConfigurationService, private pokerService: PokerService) {
    configuration.sessions
      .pipe(takeUntilDestroyed())
      .subscribe(sessions => {
        sessions?.forEach(s => this.pokerService.connect(s));
        this.sessions.next(sessions);
      });
    configuration.defaultUsername
      .pipe(takeUntilDestroyed())
      .subscribe(username => this.defaultUsername.next(username));
  }

  setDefaultUsername(defaultUsername: string) {
    this.configuration.setDefaultUsername(defaultUsername);
  }

  newSession(name: string) {
    const session = this.configuration.createNewSession(name);
    this.pokerService.connect(session);
  }

  deleteSession(session: PokerSession) {
    this.configuration.deleteSession(session);
    this.pokerService.disconnect(session);
  }

  pokerMessage(message: PokerMessage) {
    this.pokerService.sendMessage(message);
  }
}
